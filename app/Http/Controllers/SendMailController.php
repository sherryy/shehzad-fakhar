<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Session;
use Illuminate\Validation\ValidationException;
use Validator;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class SendMailController extends Controller
{
    //
    public function onSend(Request $request){

        $data = $request->all();

        $rules = [
            'name' => 'required|min:3',
            'email' => 'required|email',
            'message' => 'required|min:10'
        ];

        $validator = Validator::make($data, $rules);


        if($validator->fails()){

            throw new ValidationException($validator);

        }else{
            $vars = ['name' => Input::get('name'), 'email' => Input::get('email'), 'bodyMessage' => Input::get('message')];

            Mail::send('email.contact', $vars, function($message) use ($vars){

                $message->from($vars['email']);
                $message->to('shahzad.fakhar@ymail.com');
                $message->subject('Message via Shehzad Fakhar Portfolio');

            });
        }

//        Session::flash('message', 'Your message has been sent successfully.');
//        return redirect('/');

        $response = [
            'status' => 'success',
            'message' => 'Your message has been sent successfully',
        ];
        return response()->json([$response], 200);
    }


}
