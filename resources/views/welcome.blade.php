<!doctype html>
<html lang="en">

<head>
    <!--meta tags-->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="author" content="Designed By Shehzad Fakhar">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--template title-->
    <title>Shehzad Fakhar - Web Developer</title>



    <!--========== Theme Fonts ==========-->

    <link href="https://fonts.googleapis.com/css?family=Poppins:400,400i,600,700,800" rel="stylesheet">

    <!--Font Awesome css-->
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">

    <!--Bootstrap css-->
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">

    <!--Progress Circle css-->
    <link rel="stylesheet" href="{{asset('css/progress-circle.css')}}">

    <!--Magnific popup css-->
    <link rel="stylesheet" href="{{asset('css/magnific-popup.css')}}">

    <!--Owl carousel css-->
    <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('css/owl.theme.default.css')}}">

    <!--Normalizer css-->
    <link rel="stylesheet" href="{{asset('css/normalize.css')}}">

    <!--Template css-->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">

    <!--Responsive css-->
    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">
</head>
<body>
<!--preloader starts-->

<div class="loader_bg">
    <div class="loader"></div>
</div>

<!--preloader ends-->

<!--navigation area starts-->

<header class="nav-area navbar-fixed-top">
    <div class="container">
        <div class="row">
            <!--main menu starts-->

            <div class="col-md-12">
                <div class="main-menu">
                    <div class="navbar navbar-cus">
                        <div class="navbar-header">
                            <a href="index-2.html" class="navbar-brand"><span>S</span>hehzad <span>F</span>akhar</a>
                            <button type="button" class="navbar-toggle" data-toggle="collapse"
                                    data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>

                        <div class="navbar-collapse collapse">
                            <nav>
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="active"><a class="smooth-menu" href="#home">Home</a></li>
                                    <li><a class="smooth-menu" href="#about">About</a></li>
                                    <li><a class="smooth-menu" href="#services">Services</a></li>
                                    <li><a class="smooth-menu" href="#portfolio">Portfolio</a></li>
                                    <li><a class="smooth-menu" href="#contact">Contact</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

            <!--main menu ends-->
        </div>
    </div>
</header>

<!--navigation area ends-->

<!--Slider area starts-->

<div id="home"></div>
<div class="banner-area" id="slider-area">
    <div id="particles-js"></div>
    <div class="banner-table">
        <div class="banner-table-cell">
            <div class="welcome-text">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Shehzad Fakhar</h2> <!--edit here-->
                            <p class="name-line"></p>
                            <br>
                            <h3 class="text-affect">Freelancer <span></span> Web Developer</h3>  <!--edit here-->
                            <p class="banner-btn animate-button"><a href="#contact" class="custom-btn">Get Started</a>
                            </p>
                            <div class="clearfix"></div>
                            <div class="mouse-icon hidden-sm hidden-xs">
                                <div class="wheel"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--slider area ends-->

<!--About Area Starts-->

<div class="about-area section-padding" id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-header">
                    <h2 class="first-headline">About</h2>
                    <h3 class="second-headline">My Biodata</h3>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-xs-12 col-sm-5 col-md-4 col-lg-4 proPic">
                <img src="{{asset('images/about/about.jpg')}}" alt="about image"> <!--add your image here-->
            </div>

            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7 about-text">

                <h4>Hi! I am Shehzad Fakhar</h4> <!--edit here-->
                <p>I am a Freelancer & Back-end Developer, working for the last 2 years. I have experience working
                    with local clients and with varoius government organizations. I have vast knowledge in html, css,
                    Javascript , JQuery, Laravel, Ajax , Octobercms (Content Management System), Voyager and so on. The
                    work
                    I provide is of highest quality, fully responsive, and tested in a wide range of devices.</p>
                <!--edit here-->

                <a class="cv-btn" href="{{asset('images/about/demo-cv.png')}}" download><i class="fa fa-download"></i>Download
                    CV</a> <!--give your cv in png,jpg or pdf format-->

            </div>
        </div>
    </div>
</div>

<!--About Area Ends-->

<!--Skills Area Starts-->

<div class="skills-area">
    <div class="overlay section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-header">
                        <h2 class="first-headline">About</h2>
                        <h3 class="second-headline">My Skills</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="programming-area">
                        <div class="row">
                            <h2>Programming Skills</h2>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="single-program">
                                    <div class="progress-circle progress-90"><span>90</span></div> <!--edit here-->
                                    <h3>HTML/CSS</h3> <!--edit here-->
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="single-program">
                                    <div class="progress-circle progress-70"><span>70</span></div> <!--edit here-->
                                    <h3>Javascript</h3> <!--edit here-->
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="single-program">
                                    <div class="progress-circle progress-70"><span>70</span></div> <!--edit here-->
                                    <h3>Jquery</h3> <!--edit here-->
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="single-program">
                                    <div class="progress-circle progress-70"><span>70</span></div> <!--edit here-->
                                    <h3>Ajax</h3> <!--edit here-->
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="single-program">
                                    <div class="progress-circle progress-75"><span>75</span></div> <!--edit here-->
                                    <h3>Bootstrap</h3> <!--edit here-->
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="single-program">
                                    <div class="progress-circle progress-80"><span>80</span></div> <!--edit here-->
                                    <h3>PHP</h3> <!--edit here-->
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="single-program">
                                    <div class="progress-circle progress-65"><span>65</span></div> <!--edit here-->
                                    <h3>Laravel</h3> <!--edit here-->
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="single-program">
                                    <div class="progress-circle progress-90"><span>90</span></div> <!--edit here-->
                                    <h3>October CMS</h3> <!--edit here-->
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="single-program">
                                    <div class="progress-circle progress-95"><span>95</span></div> <!--edit here-->
                                    <h3>Voyager</h3> <!--edit here-->
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <div class="single-program">
                                    <div class="progress-circle progress-90"><span>90</span></div> <!--edit here-->
                                    <h3>Git</h3> <!--edit here-->
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-sm-6">
                    <div class="language-skills">
                        <div class="row">
                            <h2>Language Skills</h2>
                            <div class="col-md-12">
                                <div class="skillbar" data-percent="65%"> <!--edit here-->
                                    <div class="skillbar-title">Japanese</div> <!--edit here-->
                                    <div class="skill-bar-percent">30%</div> <!--edit here-->
                                    <div class="skillbar-bar" style="width: 65%;"></div> <!--edit here-->
                                </div>

                                <div class="skillbar" data-percent="98%"> <!--edit here-->
                                    <div class="skillbar-title">English</div> <!--edit here-->
                                    <div class="skill-bar-percent">98%</div> <!--edit here-->
                                    <div class="skillbar-bar" style="width: 98%;"></div> <!--edit here-->
                                </div>

                                <div class="skillbar" data-percent="98%"> <!--edit here-->
                                    <div class="skillbar-title">Urdu</div> <!--edit here-->
                                    <div class="skill-bar-percent">100%</div> <!--edit here-->
                                    <div class="skillbar-bar" style="width: 100%;"></div> <!--edit here-->
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Skills Area Ends-->

<!--Resume area starts-->

<div class="resume-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-header">
                    <h2 class="first-headline">About</h2>
                    <h3 class="second-headline">My Resume</h3>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="education-area">
                    <h2 class="text-center">Education</h2>
                    <div class="timeline">
                        <div class="wrapper left">
                            <div class="content">
                                <h3>Bachelor Degree</h3> <!--edit here-->
                                <h4>Indus University</h4> <!--edit here-->
                                <h5>2013-2016</h5> <!--edit here-->
                                <!--edit here-->
                            </div>
                        </div>
                        <div class="wrapper right">
                            <div class="content">
                                <h3>Intermediate</h3> <!--edit here-->
                                <h4>Government Degree College Malir Cantt</h4> <!--edit here-->
                                <h5>2011-2012</h5> <!--edit here-->
                                <!--edit here-->
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="experience-area">
                    <h2 class="text-center">Experience</h2>
                    <div class="timeline">
                        <div class="wrapper left">
                            <div class="content">
                                <h3>Back-end Developer</h3> <!--edit here-->
                                <h4>Sindh Police I.T Branch</h4> <!--edit here-->
                                <h5>2017 - Present</h5> <!--edit here-->
                                <p>I am currently working as a Trainee on behalf of Sindh Secretariat Implementation
                                    and coordination wing SGA & CA Government of Sindh</p>
                                <!--edit here-->
                            </div>
                        </div>
                        <div class="wrapper right">
                            <div class="content">
                                <h3>Web Developer</h3> <!--edit here-->
                                <h4>Freelancer</h4> <!--edit here-->
                                <h5>2016 - Present</h5> <!--edit here-->
                                <p>I have experience working with local clients.</p>
                                <!--edit here-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--resume area ends-->

<!--hire me area starts-->

<div class="hire-me-area">
    <div class="overlay section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                    <div class="single-hire-me">
                        <h2>Hire me for your project</h2>
                        <p>I am available for Freelance Projects. Hire me and get your project Done.</p>
                        <a href="#contact" class="animate-button custom-btn-two">Hire me</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--hire me area ends-->

<!--Services Area Starts-->

<div id="services" class="services-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-header">
                    <h2 class="first-headline">Services</h2>
                    <h3 class="second-headline">What I Offer</h3>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="single-services service-left">
                    <div class="row">
                        <div class="col-md-7 col-md-offset-2">
                            <div class="services-text-left">
                                <h4>Responsive design</h4> <!--edit here-->
                                <p>I can create Responsive design for your Need Base . HTML , CSS , Bootstrap .</p>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="service-icon">
                                <i class="fa fa-code"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="single-services service-right">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="service-icon">
                                <i class="fa fa-pencil"></i>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="services-text-right">
                                <h4>WEB DEVELOPMENT</h4> <!--edit here-->
                                <p>I can create custom websites and Content management system for your Need Base .
                                    Laravel , Octobercms ,
                                    Voyager .</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row middle-row">
            <div class="col-md-6 col-sm-6">
                <div class="single-services service-left">
                    <div class="row">
                        <div class="col-md-7 col-md-offset-2">
                            <div class="services-text-left">
                                <h4>Support</h4> <!--edit here-->
                                <p>Need Support ? Really ? If anyone wants my consultancy serives approach through my
                                    contact. I'll be happy to support you.</p>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="service-icon">
                                <i class="fa fa-life-ring"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>
</div>

<!--Services Area Ends-->

<!--Statistic area starts-->

<div class="statistic-area">
    <div class="overlay section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="single-count count-one">
                        <i class="fa fa-clock-o"></i>
                        <h2>14</h2> <!--edit here-->
                        <p>Cup of coffee a day</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="single-count count-two">
                        <i class="fa fa-code"></i>
                        <h2>657</h2> <!--edit here-->
                        <p>lines of Code</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="single-count count-three">
                        <i class="fa fa-bar-chart"></i>
                        <h2>100%</h2> <!--edit here-->
                        <p>Success</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Statistic area ends-->

<!--Portfolio Area Starts-->

<div id="portfolio" class="portfolio section-padding">
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <div class="section-header">
                    <h2 class="first-headline">Portfolio</h2>
                    <h3 class="second-headline">My Works</h3>
                </div>
            </div>
        </div>

        <div class="row">

            <ul class="portfolio-sorting list-inline text-center">
                <li><a href="#" data-group="all" class="active">All</a></li>
                <li><a href="#" data-group="php">PHP</a></li>
                <li><a href="#" data-group="laravel">Laravel</a></li>
                <li><a href="#" data-group="yii">Yii</a></li>
            </ul> <!--end portfolio sorting -->

            <div class="portfolio-items list-unstyled" id="grid">


                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["php"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/1.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/1.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["php"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/2.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/2.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["php"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/3.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/3.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["php"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/4-0.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/4-0.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["php"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/4.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/4.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["php"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/5.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/5.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["php"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/6.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/6.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["php"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/7.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/7.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["php"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/8.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/8.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["php"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/9.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/9.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["php"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/10.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/10.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["php"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/11.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/11.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["php"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/12.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/12.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["php"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/13.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/13.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["php"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/14.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/14.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["php"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/15.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/15.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["laravel"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/16.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/16.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["laravel"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/17.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/17.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["laravel"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/18.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/18.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["laravel"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/19.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/19.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["laravel"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/20.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/20.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["yii"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/21.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/21.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["yii"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/22.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/22.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["yii"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/23.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/23.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["yii"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/24.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/24.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["yii"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/25.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/25.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["yii"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/26.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/26.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-12" data-groups='["yii"]'>
                    <figure class="portfolio-item">
                        <a href="{{asset('images/portfolio/27.png')}}" class="galleryItem">
                            <img src="{{asset('images/portfolio/small/27.png')}}" alt="portfolio image"
                                 class="img-responsive"> <!--here you can change image-->
                            <span class="item-overlay">
                                        <i class="fa fa-search-plus"></i>
                                    </span>
                        </a>

                    </figure>
                </div>


            </div> <!--end portfolio grid -->


        </div> <!--end row -->
    </div> <!-- end container-->
</div>

<!--Portfolio Area Ends-->


<div id="contact" class="contact-area">
    <div class="overlay section-padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-header">
                        <h2 class="first-headline">Contact</h2>
                        <h3 class="second-headline">Get in Touch</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 box-contact-form">

                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>

                    {!! Form::open(['method'=>'POST', 'url'=>'contact', 'id'=>'contact-form']) !!}

                    <div class="messages">
                    </div>


                    <div class="controls">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::text('name', null, ['class'=>'form-control', 'id'=>'form_name', 'placeholder'=>"Enter your full name *", 'required'=>"required", 'data-error'=>"Fullname is required."]) !!}
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    {!! Form::email('email', null, ['class'=>'form-control', 'id'=>'form_email', 'placeholder'=>"Enter your email *", 'required'=>"required", 'data-error'=>"Valid email is required."]) !!}
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    {!! Form::textarea('message', null, ['class'=>'form-control', 'rows'=>4 ,'id'=>'form_message', 'placeholder'=>"Your Message *", 'required'=>"required", 'data-error'=>"Leave a message for me"]) !!}
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <button class="btn btn-send animate-button">Send message <img id="loading" style="display: none" height="30px" width="30px"   src="{{asset('images/loader/loader.gif')}}" alt="loader"></button>

                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}


                </div>

            </div>
        </div>
    </div>
</div>

<!--Contact Area Ends-->

<!--Brand logo area starts-->

<div class="brand-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-header">
                    <h3 class="second-headline">Clients</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div id="brand-carousel" class="owl-carousel owl-theme">
                <!--first logo-->
                <div class="item">
                    <div class="single-logo">
                        <img src="{{asset('images/brand/brand-1.png')}}" alt="brand image"> <!--edit here-->
                    </div>
                </div>
                <!--second logo-->
                <div class="item">
                    <div class="single-logo">
                        <img src="{{asset('images/brand/brand-2.png')}}" alt="brand image"> <!--edit here-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Brand logo area ends-->

<!--Footer Top Area starts-->

<div class="footer-top-area">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="single-footer-top">
                    <span><i class="fa fa-map-marker"></i></span>
                    <h2>Pakistan, Sindh, Karachi </h2> <!--edit here-->
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="single-footer-top">
                    <span><i class="fa fa-envelope"></i></span>
                    <h2>shahzad.fakhar@ymail.com</h2> <!--edit here-->
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="single-footer-top">
                    <span><i class="fa fa-phone"></i></span>
                    <h2>Skype: shehzad.fakhar2</h2> <!--edit here-->
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="single-footer-top">
                    <span><i class="fa fa-globe"></i></span>
                    <h2>http://shehzadfakhar.azurewebsites.net/</h2> <!--edit here-->
                </div>
            </div>
        </div>
    </div>
</div>

<!--Footer Top Area ends-->

<!--Footer Area Starts-->

<div class="footer-area">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                <p>2018 &copy; All Right Reserved By <a href="https://www.facebook.com/shehzadfakhar.jr" target="_blank">Shehzad Fakhar</a></p> <!--edit here-->
            </div>
        </div>
    </div>
</div>

<!--Footer Area Ends-->


<!--Latest version JQuery-->
<script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>

<!--Bootstrap js-->
<script src="{{asset('js/bootstrap.min.js')}}"></script>

<!--Magnific popup js-->
<script src="{{asset('js/jquery.magnific-popup.js')}}"></script>

<!--Owl Carousel js-->
<script src="{{asset('js/owl.carousel.js')}}"></script>

<!--Shuffle js-->
<script src="{{asset('js/shuffle.min.js')}}"></script>

<!--Validator js-->
<script src="{{asset('js/validator.js')}}"></script>

<!--Contact js-->
<script src="{{asset('js/contact.js')}}"></script>

<script src="{{asset('js/typed.js')}}"></script>

<!--Main js-->
<script src="{{asset('js/main.js')}}" type="text/javascript"></script>


</body>

</html>
