$(function () {

    // init the validator
    // validator files are included in the download package
    // otherwise download from http://1000hz.github.io/bootstrap-validator

    $('#contact-form').validator();


    //when the form is submitted
    $('#contact-form').on('submit', function (e) {



        $.ajaxSetup({
            header:$('meta[name="_token"]').attr('content')
        });

        // if the validator does not prevent form submit
        if (!e.isDefaultPrevented()) {

            $('#loading').show();

            var url = "contact";

            // POST values in the background the the script URL
            $.ajax({
                type: "POST",
                url: url,
                data: $(this).serialize(),
                dataType: 'json',
                success: function (response)
                {

                    // data = JSON object that contact.php returns

                    // we recieve the type of the message: success x danger and apply it to the
                    var messageAlert = 'alert-' + response[0].status;
                    var messageText = response[0].message;

                    // var messageAlert = 'alert-success';
                    // var messageText = 'Sent Successfully';



                    $('#loading').fadeOut('slow');
                    // let's compose Bootstrap alert box HTML
                    var alertBox = '<div class="alert ' + messageAlert + ' alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + messageText + '</div>';

                    console.log(alertBox);

                    // If we have messageAlert and messageText
                    if (messageAlert && messageText) {
                        // inject the alert to .messages div in our form
                        $('#contact-form').find('.messages').html(alertBox);
                        // empty the form
                        $('#contact-form')[0].reset();
                    }
                }
            });
            return false;
        }
    })
});